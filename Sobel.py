import cv2
import sys
import numpy
from scipy import misc
import matplotlib.pyplot as plt
import math



print(cv2.__version__)

if len(sys.argv) > 1:
    src = str(sys.argv[1])
    print(str(sys.argv[1]))
    img = cv2.imread(src, cv2.IMREAD_UNCHANGED)
else:
    src = cv2.VideoCapture(0)
    ret, img = src.read()

#print("Camera opened : " + str(cap.isOpened()))





gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

(retVal, grayS)= cv2.threshold(gray, 40 , 255, cv2.THRESH_TOZERO)

derivX = cv2.Sobel(grayS, ddepth=-1, dx=1, dy=0)
abs_derivX = numpy.absolute(derivX)
derivY = cv2.Sobel(grayS, ddepth=-1, dx=0, dy=1)
abs_derivY = numpy.absolute(derivY)
G = cv2.addWeighted(abs_derivX,0.50, abs_derivY, 0.50, 0 )



while (True):
    if len(sys.argv) == 1:
        ret, img = src.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        (retVal, grayS) = cv2.threshold(gray, 40, 255, cv2.THRESH_TOZERO)
        derivX = cv2.Sobel(grayS, ddepth=-1, dx=1, dy=0)
        abs_derivX = numpy.absolute(derivX)
        derivY = cv2.Sobel(grayS, ddepth=-1, dx=0, dy=1)
        abs_derivY = numpy.absolute(derivY)
        G = cv2.addWeighted(abs_derivX,0.75, abs_derivY, 0.25, 0 )
       # G = numpy.absolute(gradient)



    cv2.imshow('Originale', gray)
    cv2.imshow('Sobel de premier ordre en X', derivX)
    cv2.imshow('Sobel de premier ordre en Y', derivY)
    cv2.imshow('Sobel X + Y', G)



    key = cv2.waitKey(1)
    if key == 27:
        break







src.release()
cv2.destroyAllWindows()
